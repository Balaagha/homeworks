function Tabs(tabMenuId,tabContentId) {
    if (!(this instanceof Tabs)){ return new Tabs(tabMenuId,tabContentId);}
    this.tabMenu=document.getElementById(tabMenuId);
    this.tabContent=document.getElementById(tabContentId);
}
Tabs.prototype.hideAll = function(){
    for (let tab of this.tabMenu.children){ tab.classList.remove("active"); }
    for (let content of this.tabContent.children){ content.classList.remove("active"); }
}
Tabs.prototype.tabsactive = function () {
    this.tabMenu.addEventListener('click', (event) => {
        if (!event.target.classList.contains('active')){
            this.hideAll();
            event.target.classList.add('active');
            document.getElementById(event.target.dataset.content).classList.add('active');
        }
    });
}
const tabs = new Tabs('services-tab','tab-content');
tabs.tabsactive();


const _TABS_CONTENT = Object.freeze(  [
    {imgSrc: './img/wordpress/wordpress1.jpg',title: 'Wp project 1',category: 'Wordpress'},
    {imgSrc: './img/wordpress/wordpress2.jpg',title: 'Wp project 2',category: 'Wordpress'},
    {imgSrc: './img/wordpress/wordpress3.jpg',title: 'Wp project 3',category: 'Wordpress'},
    {imgSrc: './img/wordpress/wordpress4.jpg',title: 'Wp project 4',category: 'Wordpress'},
    {imgSrc: './img/wordpress/wordpress5.jpg',title: 'Wp project 5',category: 'Wordpress'},
    {imgSrc: './img/wordpress/wordpress6.jpg',title: 'Wp project 6',category: 'Wordpress'},
    {imgSrc: './img/wordpress/wordpress7.jpg',title: 'Wp project 7',category: 'Wordpress'},
    {imgSrc: './img/wordpress/wordpress8.jpg',title: 'Wp project 8',category: 'Wordpress'},
    {imgSrc: './img/wordpress/wordpress9.jpg',title: 'Wp project 9',category: 'Wordpress'},
    {imgSrc: './img/wordpress/wordpress10.jpg',title: 'Wp project 10',category: 'Wordpress'},
    {imgSrc: './img/graphicdesign/graphic-design1.jpg',title: 'GD project 1',category: 'Graphic Design'},
    {imgSrc: './img/graphicdesign/graphic-design2.jpg',title: 'GD project 2',category: 'Graphic Design'},
    {imgSrc: './img/graphicdesign/graphic-design3.jpg',title: 'GD project 3',category: 'Graphic Design'},
    {imgSrc: './img/graphicdesign/graphic-design4.jpg',title: 'GD project 4',category: 'Graphic Design'},
    {imgSrc: './img/graphicdesign/graphic-design5.jpg',title: 'GD project 5',category: 'Graphic Design'},
    {imgSrc: './img/graphicdesign/graphic-design6.jpg',title: 'GD project 6',category: 'Graphic Design'},
    {imgSrc: './img/graphicdesign/graphic-design7.jpg',title: 'GD project 7',category: 'Graphic Design'},
    {imgSrc: './img/graphicdesign/graphic-design8.jpg',title: 'GD project 8',category: 'Graphic Design'},
    {imgSrc: './img/graphicdesign/graphic-design9.jpg',title: 'GD project 9',category: 'Graphic Design'},
    {imgSrc: './img/graphicdesign/graphic-design10.jpg',title: 'GD project 10',category: 'Graphic Design'},
    {imgSrc: './img/graphicdesign/graphic-design11.jpg',title: 'GD project 11',category: 'Graphic Design'},
    {imgSrc: './img/graphicdesign/graphic-design12.jpg',title: 'GD project 12',category: 'Graphic Design'},
    {imgSrc: './img/webdesign/web-design1.jpg',title: 'WD project 1',category: 'Web Design'},
    {imgSrc: './img/webdesign/web-design2.jpg',title: 'WD project 2',category: 'Web Design'},
    {imgSrc: './img/webdesign/web-design3.jpg',title: 'WD project 3',category: 'Web Design'},
    {imgSrc: './img/webdesign/web-design4.jpg',title: 'WD project 4',category: 'Web Design'},
    {imgSrc: './img/webdesign/web-design5.jpg',title: 'WD project 5',category: 'Web Design'},
    {imgSrc: './img/webdesign/web-design6.jpg',title: 'WD project 6',category: 'Web Design'},
    {imgSrc: './img/webdesign/web-design7.jpg',title: 'WD project 7',category: 'Web Design'},
    {imgSrc: './img/landingpage/landing-page1.jpg',title: 'LD project 1',category: 'Landing Pages'},
    {imgSrc: './img/landingpage/landing-page2.jpg',title: 'LD project 2',category: 'Landing Pages'},
    {imgSrc: './img/landingpage/landing-page3.jpg',title: 'LD project 3',category: 'Landing Pages'},
    {imgSrc: './img/landingpage/landing-page4.jpg',title: 'LD project 4',category: 'Landing Pages'},
    {imgSrc: './img/landingpage/landing-page5.jpg',title: 'LD project 5',category: 'Landing Pages'},
    {imgSrc: './img/landingpage/landing-page6.jpg',title: 'LD project 6',category: 'Landing Pages'},
    {imgSrc: './img/landingpage/landing-page7.jpg',title: 'LD project 7',category: 'Landing Pages'}
]);
function TabsFilter(tabMenuId,tabContentId,contents) {
    if (!(this instanceof TabsFilter)){ return new TabsFilter(tabMenuId,tabContentId);}
    this.tabMenuId=tabMenuId;
    this.tabContentId=tabContentId;
    this.tabMenu=document.getElementById(tabMenuId);
    this.tabContent=document.getElementById(tabContentId);
    this.contents = contents;
}
TabsFilter.prototype.changeTab = function(el){
    for (let tab of this.tabMenu.children){ tab.classList.remove("active"); }
    el.classList.add('active');
    this.tabContent.innerHTML = '';
}
TabsFilter.prototype.filtering = function(catName){
   this.filteringContent = this.contents.filter(function (i,e,d) { if (i.category == catName){ return true; } });
}
TabsFilter.prototype.createAlltabsContent  = function  (count=8){
    this.uniqueAllcontent=[];
    this.filteringContent = [];
    while(this.uniqueAllcontent.length < count){
        let r = Math.floor(Math.random() * (this.contents.length - 1))
        if(this.uniqueAllcontent.indexOf(r) === -1) this.uniqueAllcontent.push(r);
    }
    for (let el of this.uniqueAllcontent){
        this.filteringContent.push(this.contents[el]);
    }
}
TabsFilter.prototype.addContentDom = function() {
     this.filteringContent.forEach(tabItem => {
         const {imgSrc, title, category} = tabItem,
             container = document.createElement('div'),
             img = document.createElement('img'),
             overlayContainer = document.createElement('div'),
             overlayIconContainer  = document.createElement('div'),
             overlayIconLink =  document.createElement('i'),
             overlayIconSearch = document.createElement('i'),
             overlayTextContainer  = document.createElement('div'),
             text1 = document.createElement('p'),
             text2 = document.createElement('p');
         container.classList.add('img-wrapper');
         img.className = 'ourwork-tab-img img-fluid';img.src = imgSrc;
         overlayContainer.classList.add('overlay-img');
         overlayIconContainer.classList.add('overlay-icon');
         overlayIconLink.className = 'fas fa-link';
         overlayIconSearch.className = 'fas fa-search';
         overlayTextContainer.classList.add('overlay-text');
         text1.classList.add('text1'); text1.innerText =title;
         text2.classList.add('text2'); text2.innerText = category;
         overlayTextContainer.append(text1,text2);
         overlayIconContainer.append(overlayIconLink,overlayIconSearch);
         overlayContainer.append(overlayIconContainer,overlayTextContainer);
         container.append(img,overlayContainer);
         this.tabContent.append(container);
     });
}

const loadMore = document.getElementById('loadmore');loadMore.dataset.count = 0;
const loader = document.createElement('div'); loader.classList.add('loader-wrapper');
loader.innerHTML='<div class="lds-ring"><div></div><div></div><div></div><div></div></div>';
const tabfilter = new TabsFilter('ourwork-tab','ourwork-tab-content',_TABS_CONTENT);
tabfilter.createAlltabsContent();
tabfilter.addContentDom();
tabfilter.tabMenu.addEventListener('click', (event) => {
    if (!event.target.classList.contains('active')){
        tabfilter.changeTab(event.target);
        if(event.target.dataset.content != 'all'){
            loadMore.classList.remove('active');
            tabfilter.filtering(event.target.dataset.content);
            tabfilter.addContentDom();
        } else{
            loadMore.classList.add('active');
            loadMore.dataset.count = 0;
            tabfilter.createAlltabsContent();
            tabfilter.addContentDom();
        }
    }
});
loadMore.addEventListener('click',function () {
    tabfilter.tabContent.append(loader);
    setTimeout(function () {
        loader.parentNode.removeChild(loader);
        tabfilter.createAlltabsContent(12);
        tabfilter.addContentDom();
        loadMore.dataset.count++;
        console.log(loadMore.dataset.count);
        if (loadMore.dataset.count == 2) loadMore.classList.remove('active');
    },1000);
});



