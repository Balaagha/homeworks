Javascript is a functional language meaning that functions are the primary modular units of execution. 
Functions are the main building blocks of the program.They allow the code to be called many times without repetition.

JavaScript does not throw an error if the number of arguments passed during a function invocation are different than the number of parameters listed during function definition. 
This should make it clear that parameters and arguments should be treated as two different entities. And also we can pass arbitrary data to functions using arguments.
If a function changes one of its parameters, the change is not seen outside, because  a function always gets a copy of the value.
