let num1 = prompt("Please enter a number1 ");
let num2 = prompt("Please enter a number2 ");
let operation = prompt("Please enter a operations ");
while (isNaN(num1) || num1.trim() ==="" || isNaN(num2) || num2.trim() ==="" || (operation==='/' && Number(num2)===0) ){
    num1 = prompt("Please enter the correct number format for number1");
    num2 = prompt("Please enter the correct number format for number2");
}

function calc(num1,num2,operation) {
    num1=Number(num1);
    num2=Number(num2);
    switch (operation) {
        case '+': return `${num1} + ${num2} = ${num1+num2}`;break;
        case '-': return `${num1} - ${num2} = ${num1-num2}`;break;
        case '*': return `${num1} * ${num2} = ${num1*num2}`;break;
        case '/': return `${num1} / ${num2} = ${num1/num2}`;break;
        default : return `${num1} + ${num2} = ${num1+num2}`;
    }
}

console.log(calc(num1,num2,operation));