VAR variables belong to the global scope or local scope if they are declared inside a function.The global var variables are added to the global object as properties.The var keyword allows you to redeclare a variable without any issue.
___________________________________________________________

LET variables are blocked scopes.The let variables are not added to the global object.If you redeclare a variable with the let keyword, you will get an error.
___________________________________________________________

Both of them are then accessed outside the function block. Var will work but the variable declared using let will show an error because let is block scoped.

______________________________________________________________
CONST is almost exactly the same as let. However, the only difference is that once you’ve assigned a value to a variable using const, you can’t reassign it to a new value.

