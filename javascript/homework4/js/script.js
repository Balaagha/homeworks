const arr = [true, false, null, "strings", "55", 55];
let typeIgnore = prompt("Enter type");
function filterBy(array, filter) {
    const newArr = [];
    switch (filter) {
        case 'String':
            array.forEach( function(e){ if (typeof e !== 'string') { newArr.push(e); } });
            break;
        case 'Number':
            array.forEach(function(e){ if (typeof e !== 'number') { newArr.push(e); } });
            break;
        case 'Boolean':
            array.forEach(function(e){ if (typeof e !== 'boolean') { newArr.push(e);}  });
            break;
        case 'Null':
            array.forEach(function(e){ if (typeof e !== 'object') { newArr.push(e); } });
    }
    return newArr;
}

console.log(filterBy(arr, typeIgnore));


