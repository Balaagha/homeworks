Array.prototype.forEach(callback([value, index, array]), thisArg)
This method is a member of the array prototype and uses a callback function for you to embed any custom logic to the iteration.
The forEach callback function actually accepts three arguments: element value,element index,array being traversed

