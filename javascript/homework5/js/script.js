const tabContent = document.querySelectorAll('.tabs-content li');
const tabBtn = document.querySelectorAll('.tabs li');

function hideAll(tabBtn,tabContent){
    for (let button of tabBtn){ button.classList.remove("active"); }
    for (let content of tabContent){ content.hidden = "true"; }
}
function showSelected(el) { document.getElementById(el).hidden = false; }

document.querySelector('.tabs').addEventListener('click',function (e) {
    if (!e.target.classList.contains("active")){
        hideAll(tabBtn,tabContent,);
        e.target.classList.add("active");
        showSelected(e.target.dataset.content);
    }
});