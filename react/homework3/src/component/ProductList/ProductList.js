import React, {useState} from 'react';
import './ProductList.scss';
import Product from "./Product/Product";
import Modal from '../Modal/Modal';
import {Route} from "react-router";
import '../Modal/Modal.scss';
const ProductList = (props)=>{
    const [favorites,setFavorites] = useState(JSON.parse(localStorage.getItem('favorites')));
    const [cart,setCart] = useState(JSON.parse(localStorage.getItem('carts')));
    const modalRef=React.createRef();
    const addFavHandler = (elNumber)=>{
        let fav = JSON.parse(localStorage.getItem("favorites"));
        if (fav.find(obj => obj.number === elNumber)){
            fav= fav.filter(obj => obj.number !== elNumber);
        } else {
            const result = props.data.find(obj => obj.number === elNumber);
            fav.push(result);
        }
        localStorage.setItem("favorites",JSON.stringify(fav));
        setFavorites(fav);
    }
    const addCardHandler = (data)=>{
        const carts = JSON.parse(localStorage.getItem("carts"));
        if (!carts.find(el=> el.number === data.number)){
            carts.push(data);
            localStorage.setItem("carts",JSON.stringify(carts));
            setCart(carts);
        }
    }
    const RemoveCardHandler = (data)=>{
        let carts = JSON.parse(localStorage.getItem("carts"));
        carts=carts.filter(obj => obj.number !== data.number);
        localStorage.setItem("carts",JSON.stringify(carts));
        setCart(carts);
    }
    const HomePageLoader = ()=>{
        const products = props.data.map(obj=>
            <Product favorites={favorites.find(el=> el.number === obj.number ) ? "active" : null}
                     clickHandler={modalRef} addFavHandler={addFavHandler} self={obj} key={obj.number}/>
        );
        return products;
    }
    const FavPageloader = () => {
        const products = favorites.map(obj=>
            <Product key={obj.number} favorites={favorites.find(el=> el.number === obj.number ) ? "active" : null}
                     clickHandler={modalRef} addFavHandler={addFavHandler} self={obj} />
        );
        if (products.length === 0){return (<h2>No Items</h2>);}
        return products;
    }
    const CartPageloader = () => {
        const products = cart.map(obj=>
            <Product key={obj.number} favorites={favorites.find(el=> el.number === obj.number ) ? "active" : null}
                     clickHandler={modalRef} addFavHandler={addFavHandler} self={obj} buttonName={'Delete From Cart'} />
        );
        if (products.length === 0){return (<h2>No Items</h2>);}
        return products;
    }
    return(
        <div className={"products"}>
            <Route path={'/'} exact render={HomePageLoader} />
            <Route path={'/'} exact render={()=><Modal head={"Adding product to cart"} ref={modalRef} okCondition={addCardHandler} text={"Are you sure you want to add it"}/>} />
            <Route path={'/fav'} render={FavPageloader} />
            <Route path={'/fav'} render={()=><Modal head={"Adding product to cart"} ref={modalRef} okCondition={addCardHandler} text={"Are you sure you want to add it"}/>} />
            <Route path={'/cart'} render={CartPageloader} />
            <Route path={'/cart'} render={()=><Modal head={"Deleting product product cart"} ref={modalRef} okCondition={RemoveCardHandler} text={"Are you sure you want to delete it"}/>} />

        </div>
    );
}

export default ProductList;