import React from 'react';
import './Product.scss';
const Product = (props)=>{
   const {self,addFavHandler,clickHandler,favorites,buttonName} = props;
    const url = require(`../../../../public/img/${self.path}`);
    const favIcon = React.createRef();
    const AddFavHandler = () =>{
        favIcon.current.classList.toggle('active');
        addFavHandler(self.number);
    }
    const AddCartHandler = () =>{
        clickHandler.current.openModal(self);
    }
    return(
        <div className="product-card">
            <div className="product-img-wrapper">
                    <img src={url} alt="Product"/>
                    <div className="img-overlay">
                        <i ref={favIcon} onClick={AddFavHandler} title="add to favorite" className={`fas fa-star ${favorites}`}/>
                    </div>
            </div>
            <div className="product-info-wrapper">
                <h5 className="head-text">{self.name}</h5>
                <div className="starts">
                    <i className="fas fa-star"></i>
                    <i className="fas fa-star"></i>
                    <i className="fas fa-star"></i>
                    <i className="fas fa-star"></i>
                    <i className="fas fa-star"></i>
                </div>
                <p className="info-text">Lorem ipsum dolor sit amet, con
                    adipiscing elit, sed diam nonu. </p>
                <div className="footer-info">
                    <span className="price">${self.price}</span>
                    <button onClick={AddCartHandler} className="btn-add">{buttonName ? buttonName : "Add To Cart"}</button>
                </div>
            </div>
        </div>
    );
}

export default Product;