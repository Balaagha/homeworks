import React from 'react';
import {NavLink} from "react-router-dom";
import './Header.scss'
const Header = () => {
    return (
        <div className={'nav-wrapper'}>
            <NavLink className="nav-link" exact to={"/"}>Home</NavLink>
            <NavLink className="nav-link" to={"/cart"}>Cart</NavLink>
            <NavLink className="nav-link" to={"/fav"}>Favorites</NavLink>
        </div>
    );
};

export default Header;