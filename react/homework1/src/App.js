import React from 'react';
import './App.scss';
import Modal from "./component/Modal/Modal";
import Button from "./component/Button/Button";

const App = () => {
    const modalRef1=React.useRef(),modalRef2=React.useRef();
    return (
        <div className="App">
            <Button text={"Delete"} bgColor={"red"} clickHandler={modalRef1} />
            <Button text={"Update"} bgColor={"blue"} clickHandler={modalRef2} />
            <Modal head={"Do you want to delete this file"} ref={modalRef1}
                   text={"Once you delete this file, it won’t be possible to undo this action.\nAre you sure you want to delete it"}/>
            <Modal head={"Do you want to update this file"} ref={modalRef2}
                   text={"Once you update this file, it won’t be possible to undo this action.\nAre you sure you want to update it"}/>
        </div>
    );
};

export default App;